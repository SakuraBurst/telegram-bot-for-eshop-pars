const TelegramBot = require('node-telegram-bot-api');
const Koa = require('koa');
const Router = require('koa-router');
const bodyParser = require('koa-bodyparser');
const getId = require('./parseMethod');

const ip = process.env.IP || 'localhost';
const port = process.env.PORT || 8443;
const host = 'https://telegrma-bot-with-eshop-scrap.herokuapp.com';
const TOKEN = process.env.TOKEN;

console.log('getID', getId);
let testurl =
    'https://www.nintendo.ru/-/Nintendo-Switch/Animal-Crossing-New-Horizons-1438623.html';
//asyn fuctions for get game info

console.log(TOKEN);
getId(testurl).then(result => console.log('undefinded?', result));
//declare new bot and set his url as host
const bot = new TelegramBot(TOKEN);
bot.setWebHook(`${host}/bot`);

//start server side
const app = new Koa();

const router = Router();

router.post('/bot', ctx => {
    const { body } = ctx.request;
    bot.processUpdate(body);
    console.log(ctx);
    ctx.status = 200;
});
app.use(bodyParser());
app.use(router.routes());

app.listen(port, () => {
    console.log('server listening on' + port);
});

bot.onText(/\/price/, (msg, match) => {
    const chatId = msg.chat.id;
    bot.sendMessage(
        chatId,
        'Да, знаю, это не бот твоей мечты, но какая игра-то?',
        {
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: '🚿марева📯',
                            callback_data: 'Super-Mario-Maker-2-1514009.html'
                        }
                    ],
                    [
                        {
                            text: '💸слыш купи❄️',
                            callback_data:
                                'The-Elder-Scrolls-V-Skyrim--1177455.html'
                        }
                    ],
                    [
                        {
                            text: '💩дерьмак 3 дерьмовая охота💩',
                            callback_data: '-3--1575086.html'
                        }
                    ],
                    [
                        {
                            text: '🛡огненная эмблема 3 дома(на англеском)⚔️',
                            callback_data:
                                'Fire-Emblem-Three-Houses-1175482.html'
                        }
                    ],
                    [
                        {
                            text: '⚔️test non sale pokemon sword',
                            callback_data: 'Pokemon-Sword-1522111.html'
                        }
                    ]
                ]
            }
        }
    );
});

bot.on('callback_query', query => {
    const id = query.message.chat.id;
    let url = 'https://www.nintendo.ru/-/Nintendo-Switch/' + query.data;
    getId(url).then(result => {
        let md;
        if (result.saleStatus === 'not on sale') {
            md = `NOT ON SALE 
        Название: ${result.name}
        Жанры: ${result.genre}
        Цена: ${result.oldPrice}
        `;
        } else {
            md = `
        Название: ${result.name}
        Жанры: ${result.genre}
        Старая цена: ${result.oldPrice}
        Новая цена: ${result.newPrice}
        `;
        }
        bot.sendMessage(id, md, { parse_mode: 'Markdown' });
    });
});
//bot.sendPhoto(
//`@${chanel}`,
//'https://cdn140.picsart.com/292181617076201.jpg?r1024x1024'
//);
/*
console.log(JSON.stringify(response.data));
        console.log(JSON.stringify(response.data.prices[0].regular_price));
        console.log(JSON.stringify(response.data.prices[0].discount_price));


async function getPrice() {
    try {
        const response = await axios.get(
            'https://api.ec.nintendo.com/v1/price?country=RU&lang=ru&ids=70010000003087'
        );
        if (response.data.prices[0].sales_status == 'onsale') {
            console.log(response.data);
            console.log(response.data.prices[0].regular_price);
            console.log(response.data.prices[0].discount_price);
        }
    } catch (error) {
        console.error(error);
    }
}
getPrice();
*/
module.exports = bot;
