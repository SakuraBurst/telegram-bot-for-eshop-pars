FROM node:14.15.1

WORKDIR /parser/app

COPY . .

RUN npm i

CMD ["node", "feed.js"]