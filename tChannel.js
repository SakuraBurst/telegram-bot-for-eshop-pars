require('dotenv').config();
const TelegramBot = require('node-telegram-bot-api');
const channel = process.env.TELEGRAMCHANELNAME;
const TOKEN = process.env.TOKEN;

class TChannel {
    constructor() {
        this.bot = new TelegramBot(TOKEN);
        this.channel = channel;
    }
    async sendMessage(gameCollection) {
        const lowestPrice = await gameCollection.lowestPrice()

        const message = `     ${this.getTitle(lowestPrice, gameCollection.currentPrice())}
               Название: ${gameCollection.game.game_name}
               Цена: ${gameCollection.currentPrice()} RUB
               `;

        await this.bot.sendMessage(`@${this.channel}`,message, {parse_mode: 'Markdown'})
    }
    getTitle(lowestPrice, currentGamePrice){
        let titleString
        if(!lowestPrice){
            titleString = 'Новая игра в базе данных'
        }
        else if(currentGamePrice < lowestPrice){
            titleString = 'Самая низкая цена'
        }
        return titleString
    }
}

module.exports = TChannel;
