const cheerio = require('cheerio');
const axios = require('axios').default;
require('dotenv').config()

async function getId(url) {
    try {
        const response = await axios.get(url);
        const reg = /(.gameGenre..|.gameTitle..|.offdeviceNsuID..)/;
        const $ = cheerio.load(response.data);
        const dataArr = $('script')
            .get()[1]
            .children[0].data.replace(/\s/g, '')
            .split(',')
            .filter(a => reg.test(a));
        const gameObj = {};
        gameObj.genre = dataArr[0]
            .replace(reg, '')
            .replace(/^(.)(.*)(.)$/, '$2');
        gameObj.name = dataArr[1]
            .replace(reg, '')
            .replace(/([A-Z])/g, ' $1')
            .replace(/^(.\s)(.*)(.)$/, '$2');
        gameObj.gameId = Number(
            dataArr[2].replace(reg, '').replace(/^(.)(.*)(.)$/, '$2')
        );
        const complete = await getPrice(gameObj);
        return complete;
    } catch (error) {
        console.error(error);
    }
}

async function getPrice(gameInfo) {
    try {
        const response = await axios.get(
            'https://api.ec.nintendo.com/v1/price?country=RU&lang=ru&ids=' +
                gameInfo.gameId
        );
        if (response.data.prices[0].discount_price) {
            let completeRequest = gameInfo;
            completeRequest.saleStatus = 'onSale';
            completeRequest.oldPrice =
                response.data.prices[0].regular_price.amount;
            completeRequest.newPrice =
                response.data.prices[0].discount_price.amount;
            return completeRequest;
        } else {
            let completeRequest = gameInfo;
            completeRequest.saleStatus = 'not on sale';
            completeRequest.oldPrice =
                response.data.prices[0].regular_price.amount;
            return completeRequest;
        }
    } catch (error) {
        console.error(error);
    }
}

function getPrices(json){
    return axios.post(`${process.env.PARSER_URL}/get`, json).then((resp) => {
        return resp.data
    }).catch(e => {
        console.log(e)
    })
}

module.exports = {getId, getPrices};
