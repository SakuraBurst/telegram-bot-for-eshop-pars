async function getPrice(gameInfo) {
    try {
        const response = await axios.get(
            'https://api.ec.nintendo.com/v1/price?country=RU&lang=ru&ids=' +
                gameInfo.gameId
        );
        if (response.data.prices[0].discount_price) {
            let completeRequest = gameInfo;
            completeRequest.saleStatus = 'onSale';
            completeRequest.oldPrice =
                response.data.prices[0].regular_price.amount;
            completeRequest.newPrice =
                response.data.prices[0].discount_price.amount;
            return completeRequest;
        } else {
            let completeRequest = gameInfo;
            completeRequest.saleStatus = 'not on sale';
            completeRequest.oldPrice =
                response.data.prices[0].regular_price.amount;
            return completeRequest;
        }
    } catch (error) {
        console.error(error);
    }
}

module.exports = getPrice;
