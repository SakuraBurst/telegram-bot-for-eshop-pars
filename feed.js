require('dotenv').config()
const MongoClient = require('mongodb').MongoClient;
const {getPrices} = require('./parseMethod');
const dburl = process.env.MONGODB_URI;
const TChannel  = require('./tChannel')
const GameCollection  = require('./gameCollection')

const games = process.env.GAMES.split(',').map(a => ({name: a.trim()}));
console.log(games)
function mainWorker(){
    console.log('script started at ', new Date())
    const client = new MongoClient(dburl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    client.connect(async err => {
        if (err) {
            return console.error(err);
        }
        const dataBase = client.db('eshopPars')
        const collections = await dataBase.listCollections({}, {nameOnly: true}).toArray()
        await getPrices(games).then(async response => {
            console.log(response)
            for(let i = 0; i < response.length; i++){
                let currentCollection;
                if(gameCollectionExist(collections, response[i].game_name)){
                    currentCollection = dataBase.collection(response[i].game_name)
                }
                else {
                    currentCollection = await dataBase.createCollection(response[i].game_name)
                }
                const gameCollection = new GameCollection(currentCollection, response[i])
                const tChannel = new TChannel()
                if (await gameCollection.newPostCondition()){
                    await tChannel.sendMessage(gameCollection)
                    await gameCollection.writeToTheDatabase()
                }
            }
        })
        await client.close();
    });
}


function gameCollectionExist(collectionList, gameName){
    return collectionList.some(a => a.name === gameName)
}
mainWorker()

setInterval(mainWorker, 1000 * 60 * 60)





// const games2 = [
//     {name: "Minecraft"},
//     {name: "The Elder Scrolls V: Skyrim"},
//     {name: "The Legend of Zelda: Breath of the Wild"},
//     {name: "Super Mario Odyssey"},
//     {name: "Ori and the Will of the Wisps"},
//     {name: "Super Mario Maker 2"},
//     {name: "Animal Crossing: New Horizons"},
//     {name: "Angels with Scaly Wings"},
//     {name: 'Poison Control'}
// ]

// Для истории исправленная плохая версия
// client.connect(async err => {
//     if (err) {
//         return console.error(err);
//     }
//     const bot = new TelegramBot(TOKEN);
//     const minecraftdDB = client.db('eshopPars').listCollections({}, {nameOnly: true}).toArray().then((meh) => {
//         console.log(meh)
//     })
//     const emblemDB = client.db('eshopPars').collection('emblem');
//     const crossingDB = client.db('eshopPars').collection('crossing');
//     const marioDB = client.db('eshopPars').collection('mario');
//     const skyrimDB = client.db('eshopPars').collection('skyrim');
//     const minecraftDB = client.db('eshopPars').collection('minecraft');
//
//
//     async function eshopPrices() {
//         await getId(mario).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 return marioDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         marioDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 return marioDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         marioDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         await getId(emblem).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 return emblemDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         emblemDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                         return Promise.resolve()
//                     }
//                 });
//             } else {
//                 emblemDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         emblemDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//         await getId(skyrim).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 return skyrimDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         skyrimDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 return skyrimDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         skyrimDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         await getId(crossing).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 return crossingDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         crossingDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 return crossingDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         crossingDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         await getId(minecraft).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 return minecraftDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         minecraftDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 return minecraftDB.find().toArray().then((prices) => {
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         minecraftDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//         console.log('hhhhhhhhhhhh')
//         return Promise.resolve();
//     }
//     await eshopPrices();
//     console.log('ha')
//     await client.close();
//
// });


// для истории плохая плохая версия
// client.connect(err => {
//     if (err) {
//         return console.error(err);
//     }
//     const bot = new TelegramBot(TOKEN);
//     const emblemDB = client.db('eshopPars').collection('emblem');
//     const crossingDB = client.db('eshopPars').collection('crossing');
//     const marioDB = client.db('eshopPars').collection('mario');
//     const skyrimDB = client.db('eshopPars').collection('skyrim');
//     const minecraftDB = client.db('eshopPars').collection('minecraft');
//
//     async function eshopPrices() {
//         let marivaPromice = await getId(mario).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 marioDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         marioDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 marioDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         marioDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//         let emblemPromice = await getId(emblem).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 emblemDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         emblemDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 emblemDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         emblemDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//         let skyrimPromice = await getId(skyrim).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 skyrimDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         skyrimDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 skyrimDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         skyrimDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         let crossingPromise = await getId(crossing).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 crossingDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         crossingDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 crossingDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         crossingDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         let minecraftPromice = await getId(minecraft).then(result => {
//             let info;
//             if (result.saleStatus === 'not on sale') {
//                 minecraftDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.oldPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `     Новая цена
//                     Название: ${result.name}
//                     Жанры: ${result.genre}
//                     Цена: ${result.oldPrice}
//                     `;
//                         minecraftDB.insertOne(
//                             { name: result.name, price: result.oldPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             } else {
//                 minecraftDB.find().toArray((err, prices) => {
//                     if (err) {
//                         console.error(err);
//                     }
//                     prices.sort((a, b) => {
//                         let priceA = a.price.replace(/\s\w+/, '');
//                         let priceB = b.price.replace(/\s\w+/, '');
//                         return Number(priceA) - Number(priceB);
//                     });
//                     let lowestPrice = prices[0].price.replace(/\s\w+/, '');
//                     let newPrice = result.newPrice.replace(/\s\w+/, '');
//                     if (lowestPrice > newPrice) {
//                         info = `Новая скидка
//                 Название: ${result.name}
//                 Жанры: ${result.genre}
//                 Старая цена: ${result.oldPrice}
//                 Новая цена: ${result.newPrice}
//                 `;
//                         minecraftDB.insertOne(
//                             { name: result.name, price: result.newPrice },
//                             (err, result) => {
//                                 if (err) {
//                                     return console.error(err);
//                                 }
//                                 console.log('Saved new price in database');
//                             }
//                         );
//                         bot.sendMessage(`@${chanel}`, info, {
//                             parse_mode: 'Markdown'
//                         });
//                     } else {
//                         console.log('Старая цена');
//                     }
//                 });
//             }
//         });
//
//         let response = await setTimeout(function() {
//             return client.close();
//         }, 3000);
//         return;
//     }
//     eshopPrices();
// });
