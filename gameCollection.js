class GameCollection{
    constructor(collection, game) {
        this.collection = collection
        this.game = game
        this.currentLowestPrice = null;
    }
    async lowestPrice(){
        if (this.currentLowestPrice !== null){
            return this.currentLowestPrice
        }
        const listOfPrices = await this.collection.find().toArray()
        listOfPrices.sort((a,b) => a.price - b.price)
        if (!listOfPrices[0]){
            return this.currentLowestPrice = listOfPrices[0]
        }
        this.currentLowestPrice = listOfPrices[0].price
        return listOfPrices[0].price
    }
    currentPrice(){
        if(this.isOnSale){
            return Number(this.game.game_info.prices[0].discount_price.raw_value)
        }
        else {
            return Number(this.game.game_info.prices[0].regular_price.raw_value)
        }
    }
    get isOnSale(){
        return !!this.game.game_info.prices[0].discount_price
    }
    async newPostCondition(){
        const lowestPrice = await this.lowestPrice()
        return !lowestPrice || lowestPrice > this.currentPrice()
    }
    async writeToTheDatabase(){
        await this.collection.insertOne({price: this.currentPrice()}).then(() => {
            this.currentLowestPrice = null
            return Promise.resolve()
        })
    }
}

module.exports = GameCollection